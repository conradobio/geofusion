import plotly.graph_objects as go
import pandas as pd


#___________________________________________
#1. Preprocessing
def descritive_analysis(df, list, column, title):

    df = df.copy()
    df[f'%_{column}'] = df[column] / df[list].sum(axis=1)
    print(f"{df[f'%_{column}'].mean().round(2) * 100}% de {column}")

    desc_df = df[list + [column]].agg(['min', 'max', 'mean']).round(2)
    
    fig = go.Figure()

    for col in (list + [column]):
        fig.add_trace(go.Box(y=df[col], name=col,))
    
    fig.update_layout(title=f'Distribuição de {title}',)
    fig.show()
    
    return desc_df, df

def preprocess_dataframe(df):
    df['nome'] = df['nome'].astype(str)
    df['codigo'] = df['codigo'].astype(str)

    df.loc[df['rendaMedia'] == '-', 'rendaMedia'] = 0
    df['rendaMedia'] = df['rendaMedia'].astype(float)
    df['rendaMedia'].fillna(0, inplace=True)

    df['potencial'].fillna(0, inplace=True)
    df['potencial'] = df['potencial'].map({'Baixo': 1, 'Médio': 2, 'Alto': 3})

    df['popAlvo'] = df['popDe25a34'] + df['popDe35a49']
    df['domiciliosAlvo'] = df['domiciliosA1'] + df['domiciliosA2'] + df['domiciliosB1'] + df['domiciliosB2']

    sp_df = df.query('cidade == "São Paulo"').reset_index(drop=True)
    rj_df = df.query('cidade == "Rio de Janeiro"').reset_index(drop=True)
    sp_df = sp_df.fillna(0)
    
    return df, sp_df, rj_df

def box_plot_distribution(df, cols):
    fig = go.Figure()

    for col in cols:
        fig.add_trace(go.Box(y=df[col], name=col,))

    fig.update_layout(title='Distribuição de renda e faturamento',)
    fig.show()