import pandas as pd
import numpy as np
import pickle
import matplotlib.pyplot as plt

import warnings
warnings.filterwarnings("ignore")

from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, roc_auc_score, classification_report
from sklearn.cluster import KMeans


def split_data_to_train(df, col_list, col_target):
    X = df.drop(columns=col_list)
    y = df[col_target].values

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    return X_train, X_test, y_train, y_test

def preprocess_model(X_train, X_test):
    pipeline = Pipeline([
    ('std_scalar', StandardScaler())
    ])

    X_train = pipeline.fit_transform(X_train)
    X_test = pipeline.transform(X_test)
    return X_train, X_test

def train_model(model, X_train, y_train):
    return model.fit(X_train, y_train)

def predict_model(model, X):
    return model.predict(X)

def evaluate(true, predicted):
    mae = mean_absolute_error(true, predicted)
    mse = mean_squared_error(true, predicted)
    rmse = np.sqrt(mse)
    r2_square = r2_score(true, predicted)
    return mae, mse, rmse, r2_square

def main_regression(df, model, col_list, col_target):
    X_train, X_test, y_train, y_test = split_data_to_train(df, col_list, col_target)
    X_train, X_test = preprocess_model(X_train, X_test)
    clf = train_model(model, X_train, y_train)
    y_pred = predict_model(clf, X_test)
    score = clf.score(X_train, y_train)
    return y_test, y_pred, clf, score, 

def model_dataframe(model_name, y_test, y_pred, score):
    return pd.DataFrame(data=[[model_name, *evaluate(y_test, y_pred), score]],
                         columns=['Model', 'MAE', 'MSE', 'RMSE', 'R2 SQUARE', 'SCORE'])

def save_model(path, model):
    with open(path, 'wb') as f:
        pickle.dump(model, f)

def load_model(path):
    with open(path, 'rb') as f:
        return pickle.load(f)


def predict_test_data(model, df, col_list):
    X_test_sp = df.drop(columns=col_list)
    _, X_test = preprocess_model(X_test_sp, X_test_sp)
    return model.predict(X_test)

def predicted_value_to_df(df, col, value):
    df[col] = value

    if value[value < 0].any():
        df[col] = df[col].apply(lambda x: max(x, 0))
        
    return df

def compare_predict_train_data(df, col, y_pred, numeric=True):
    name = df[col].values
    dif = (name-y_pred)/name*100 if numeric else name == y_pred
    return pd.DataFrame({'real':name, 'pred':y_pred, 'dif':dif}).\
            sort_values(by='dif', ascending=False)

#___________________________________
# 3. Classificacao Bairros

def main_classification(df, col_list, col_target):
    X_train, X_test, y_train, y_test = split_data_to_train(df, col_list, col_target)
    X_train, X_test = preprocess_model(X_train, X_test)

    log_r = train_model(LogisticRegression(max_iter=1000, fit_intercept=True, verbose=0, 
                                        solver='liblinear', random_state=42),
                        X_train, 
                        y_train)
    y_pred = predict_model(log_r, X_test)

    accuracy = accuracy_score(y_test, y_pred)
    print('Accuracy: %.2f' % (accuracy*100))

    y_score = log_r.predict_proba(X_test)
    auc = roc_auc_score(y_test, y_score, multi_class='ovr')
    print('ROCauc: %.2f' % auc)

    return log_r, y_test, y_pred 


#___________________________________
# 4. Segmentacao
def processing_data(df):
    return StandardScaler().fit_transform(df)

def find_size_inercias(df):
    inertias = []
    for k in range(1,10):
        km=KMeans(k)
        km.fit(df)
        inertias.append(km.inertia_)
    return inertias

def plot_inertias(inertias):
    plt.plot(range(1,10), inertias, marker='o')
    plt.xlabel('Number of clusters')
    plt.ylabel('Inertias')

def fit_kmeans(df, n_clusters):
    return KMeans(n_clusters=n_clusters, random_state=0).fit(df)

def main_cluster(df, df_final, n_clusters):
    df = processing_data(df)
    inertias = find_size_inercias(df)
    plot_inertias(inertias)
    km = fit_kmeans(df, n_clusters)
    save_model('../models/cluster_model.pickle', km)
    print('Model saved to ../models/cluster_model.pickle')
    df_final['cluster'] = km.labels_
    return df_final, km