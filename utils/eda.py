import plotly.graph_objects as go
import pandas as pd


#___________________________________________
#5. EDA
def box_plot(df):
    fig = go.Figure()

    for cidade in df['cidade'].unique():
        fig.add_trace(go.Box(y=df.query(f'cidade == "{cidade}"')['faturamento'], name=cidade))

    fig.update_layout(title='Distribuição de Faturamento entre as Cidades', xaxis_title='Cidade', yaxis_title='Faturamento')
    fig.show()

def bar_chart_potencial_distribution(df):
    fig = go.Figure()

    for cidade in df['cidade'].unique():
        df_filter = df.query(f'cidade == "{cidade}"')['potencial'].value_counts().\
                        reset_index().\
                        rename(columns={'index': 'Potencial', 'potencial': 'Quantidade'})

        df_filter['%'] = df_filter['Quantidade'] / df_filter['Quantidade'].sum()

        fig.add_trace(go.Bar(x=df_filter['Potencial'], 
                            y=df_filter['%'], 
                            name=cidade, 
                            text=df_filter['%'].round(2),
                            ))

    fig.update_layout(title='Distribuição de Potencial entre as Cidades', xaxis_title='Potencial', yaxis_title='Distribuição')
    fig.show()

def bar_chart_faturamento_distribution(df):
    fig = go.Figure()

    for cidade in df['cidade'].unique():
        df_filter = df.query(f'cidade == "{cidade}"').groupby('potencial')['faturamento'].sum().\
                    reset_index()
        df_filter['text'] = df_filter['faturamento'] / 1000000
        fig.add_trace(go.Bar(x=df_filter['potencial'], 
                        y=df_filter['faturamento'], 
                        name=cidade, 
                        texttemplate='%{text:.2f}',
                        text=df_filter['text'],
                        ))

    fig.update_layout(title='Distribuição de Faturamento por Potencial entre as Cidades', 
                        xaxis_title='Potencial', 
                        yaxis_title='Distribuição')
    fig.show()

def bar_chart_bairro_potencial(df, potencial):
    fig = go.Figure()

    df_filter = df.query(f'cidade == "São Paulo" and potencial == "{potencial}"')[['nome','text', 'faturamento', 'cidade']].\
                        sort_values(by='faturamento',ascending=False)[:20]

    fig.add_trace(go.Bar(x=df_filter['nome'], 
                    y=df_filter['faturamento'], 
                    text=df_filter['text'],
                    ))

    fig.update_layout(title=f'Distribuição dos maiores Faturamentos com "{potencial}" Potencial - Cidade São Paulo', 
                        xaxis_title='Bairros', 
                        yaxis_title='Faturamento')
    fig.show()

def scatter_chart_per_cluster(df, col_x, col_y):
    
    fig = go.Figure()

    for cluster in df['cluster'].unique():
        fig.add_trace(go.Scatter(
                    x=df[df['cluster'] == cluster][col_x],
                    y=df[df['cluster'] == cluster][col_y],
                    mode='markers',
                    name=str(cluster),
                    marker_color=cluster,
                    legendgroup=str(cluster),
                    customdata=df[['cidade', 'potencial']],
                    hovertemplate='<br>'.join([
                            '{col_x}: %{x}',
                            '{col_y}: %{y}',
                            'Cidade: %{customdata[0]}',
                            'Potencial: %{customdata[1]}'
                    ]),
                    ))    
    fig.update_layout(title=f'{col_x} x {col_y}', xaxis_title=col_x, yaxis_title=col_y)
    fig.show()